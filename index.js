let userName = document.getElementById("userName");
let emailId = document.getElementById("emailId");
let select = document.getElementById("select");
let movie = document.getElementById("movie");
let submit1 = document.getElementById("form");
let color = document.getElementById("color");
let range = document.getElementById("range");
let category = document.getElementsByName("category");
let popup = document.getElementById("popup");
let book = document.getElementById("book");
let whatsLike = document.getElementById("whatsLike");
let yourId = document.getElementById("yourId");
let nameVal = document.getElementById("nameVal");
var genValue = false;
let condition1 = false;
let condition2 = false;
let condition3 = false;
let condition4 = false;
let condition5 = false;
submit1.addEventListener("submit", (e) => {
  e.preventDefault();
  //   user Name
    if (userName.value.length < 4) {
        nameVal.style.color = "red";
        condition1 = false;
    }
    else {
        nameVal.style.color = "black";
        condition1 = true;
    }
  // email
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!emailId.value.match(filter)) {
        yourId.style.color = "red";
        condition2 = false;
    }
    else {
        yourId.style.color = "black";
        condition2 = true;
    }
  // select
    if (select.value == "none") {
        whatsLike.style.color = "red";
        condition3 = false;
    }
    else if (
        select.value == "Inception" ||
        select.value == "Avatar The Way Of Water" ||
        select.value == "Interstaller" ||
        select.value == "Old Boy"
    ) {
        whatsLike.style.color = "black";
        condition3 = true;
    }
  //    radio
  let count = 0;
  for (var i = 0; i < category.length; i++) {
        if (!category[i].checked) {
            count++;
            if (count >= 3) {
                condition4 = false;
                book.style.color = "red";
            }
        }
  }
  let Genre = "";
  let condition6 = false;
    category.forEach((e) => {
        if (e.checked) {
            Genre = e.value;
            condition4 = true;
            book.style.color = "black";
        }
    });
    if (Genre == "") {
        condition6 = false;
    }
    else {
        condition6 = true;
    }
  // I agree
  let Agree = document.getElementById("checkbox");
  let checkVal = document.getElementById("checkVal");
    if (!Agree.checked) {
        checkVal.style.color = "red";
        condition4 = false;
    }
    else {
            console.log("CheckedValue");
            checkVal.style.color = "black";
            condition4 = true;
    }

  let modal = document.createElement("div");
       modal.classList.add("modal");
 let  button = document.createElement("button");
       button.classList.add("close")
       button.textContent = "CLOSE";
  // close popUp
    button.addEventListener("click", () => {
        location.reload();
    });

  let btnDiv = document.createElement("div");
       btnDiv.classList.add("btnDiv");
  // head
  let Head = document.createElement("h1");
  // email
  let emailDiv = document.createElement("div");
       emailDiv.classList.add("emailDiv");
  // You love
  let movie = document.createElement("div");
       movie.classList.add("movie");
  // color
  let colorVal = document.createElement("div");
       colorVal.classList.add("colorVal");
  // Range
  let Rang = document.createElement("div");
       Rang.classList.add("Range");
  // Range
  let Select = document.createElement("div");
       Select.classList.add("Select");
  //I agrre
  let agree = document.createElement("div");
       agree.classList.add("agree");
  // Appending all chind elements
        btnDiv.appendChild(button);
        modal.appendChild(btnDiv);
        modal.appendChild(Head);
        modal.appendChild(emailDiv);
        modal.appendChild(movie);
        modal.appendChild(colorVal);
        modal.appendChild(Rang);
        modal.appendChild(Select);
        modal.appendChild(agree);

  // condiotion
    if (
        condition1 == true &&
        condition2 == true &&
        condition3 == true &&
        condition4 == true &&
        condition6 == true
        ) {
        popup.appendChild(modal);
        // rendering on Popup
        Head.textContent = "Hello " + userName.value;
        emailDiv.textContent = "Email: " + emailId.value;
        movie.textContent = "You Love:  " + select.value;
        colorVal.textContent = "Color: " + color.value;
        Rang.textContent = "Rating: " + range.value;
        Select.textContent = "Book Genre: " + Genre;
        agree.textContent = "👉 You agree to terms & conditions";
    }
});

    select.addEventListener("change", () => {
              movie.textContent = select.value;
    });
